//
//  Constants.swift
//  Heroes
//
//  Created by Arthur Cançado on 01/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit

struct Constants {
    
    struct API {
        
        static let baseURL = "http://gateway.marvel.com/v1/public/"
        
        static let privateKey = "0650ae5eb72ce6ca875181ec189f2f13ac166d82"
        
        static let publicKey = "ee47896b64a1dccc2083df85922b4032"

    }
    
    struct Fields {
        
        struct Response {
            
            static let offset = "data.offset"
            static let limit = "data.limit"
            static let total = "data.total"
            static let count = "data.count"
            static let results = "data.results"
        }
        
        struct Character {
            
            static let id = "id"
            static let name = "name"
            static let description = "description"
            static let thumbnailPath = "thumbnail.path"
            static let thumbnailExtension = "thumbnail.extension"
        }
        
        struct Event{
            
            static let id = "id"
            static let title = "title"
            static let description = "description"
            static let thumbnailPath = "thumbnail.path"
            static let thumbnailExtension = "thumbnail.extension"
            
        }
        
        
    }
    
    struct Titles{
        
        static let actionSheetTitle = "Choose the order"
        
        static let actionSheetOptionDate = "Date Modified"
        static let actionSheetOptionNameAsc = "A -> Z"
        static let actionSheetOptionNameDesc = "Z -> A"
        
        static let actionSheetOptionCancel = "Cancel"
        
        static let heroesViewTitle = "Heroes"
        static let heroDetailViewTitle = "Details"
        static let heroEventsViewTitle = "Events"
        
        static let segmentDetailsTitle = "Details"
        static let segmentEventsTitle = "Events"
        
        static let favoriteCharacters = "Favorite Characters"
        static let allCharacters = "All Characters"
        
    }
    
    struct Messages {
        
        static let noDataAvailable = "No data available"
        
        static let emptyTopMessageCharacter = ""
        static let emptyBottomMessageCharacter = "No date available"
        
        static let emptyTopMessageEvents = ""
        static let emptyBottomMessageEvents = "No events found for this hero"
        
        
    }
    
    struct Colors {
        
        static let navigationTintColor = UIColor.whiteColor()
        
        static let navigationBarTintColor = UIColor.blackColor()
        
        static let segmentControlTintColor = UIColor.redColor()
        
    }
    
}