//
//  HeroesRouter.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import Alamofire

enum HeroesRouter: URLRequestConvertible {
        
    case getHeroes(offset: Int, name: String, orderBy: String)
    case getHero(Int)
    case getHeroEvents(id: Int, offset: Int)
    
    var method: Alamofire.Method {
        switch self {
        case .getHeroes:
            return .GET
        case .getHero:
            return .GET
        case .getHeroEvents:
            return .GET
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        
        var ts: String =  NSDate().timeIntervalSince1970.description
        
        var hash: String {
            return (ts+Constants.API.privateKey+Constants.API.publicKey).md5()
        }
        
        
        let result: (path: String, parameters: [String: AnyObject]) = {
            
            switch self {
                
            case .getHeroes(let offset, let name, let orderBy):
                
                var params: [String: AnyObject] = ["ts" : ts, "apikey": Constants.API.publicKey, "hash": hash, "offset" : offset, "orderBy" : orderBy]
                
                if name != ""{
                    params["name"] = name
                }
                
                return ("characters", params )
                
            case .getHero(let id):
                return ("characters/\(id)", ["ts" : ts, "apikey": Constants.API.publicKey, "hash": hash])
                
            case .getHeroEvents(let id, let offset):
                return ("characters/\(id)/events", ["ts" : ts, "apikey": Constants.API.publicKey, "hash": hash, "offset" : offset])
                
            }
        }()
        
        let URL = NSURL(string: Constants.API.baseURL)!
        
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        mutableURLRequest.HTTPMethod = method.rawValue
        
        let encoding = Alamofire.ParameterEncoding.URLEncodedInURL
        
        let aux = encoding.encode(mutableURLRequest, parameters: result.parameters).0
        return aux
    }

}
