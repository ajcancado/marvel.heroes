//
//  FavoriteHeroProtocolViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 02/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit

protocol FavoriteHeroProtocol{
        
    func favoriteCharacter(favoriteButton: UIButton)
}

