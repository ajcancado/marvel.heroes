//
//  String+Extension.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import Foundation

extension String {
    
    func md5() -> String {
        
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }

}
