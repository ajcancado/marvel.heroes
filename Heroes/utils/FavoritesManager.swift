//
//  PersistenceManager.swift
//  Heroes
//
//  Created by Arthur Cançado on 01/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit

class FavoritesManager {
    
    var characters: [Character]!
    
    static let get = FavoritesManager()
    
    private init() {
        
        characters = retrieveFavoriteCharacters()
    }
    
    struct Keys {
        
        static let favorites = "favorites"
        
    }
    
    func removeFavoriteChracter(character: Character){
        
        var index: Int?
        
        for i in (0..<characters.count) {
            if characters[i].name == character.name {
                index = i
            }
        }
        
        if index != nil {
            
            characters.removeAtIndex(index!)
            
            saveInPreferences()
            
        }
        else{
            print("Erro ao encontrar o favorito")
        }
    }
    
    func saveFavoriteCharacter(character: Character){
        
        characters.append(character)
        
        saveInPreferences()
    }
    
    func saveInPreferences(){
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(characters)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: Keys.favorites)
    }
    
    func containsCharacter(character: Character) -> Bool {
        
        for i in (0..<characters.count) {
            if characters[i].name == character.name {
                
                return true
                
            }
        }
        
        return false
    }
    
    func retrieveFavoriteCharacters() -> [Character] {
        
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(Keys.favorites) as? NSData {
            
            let characters = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [Character]
            
            return characters
        }
        
        return []
        
    }
    
    
}
