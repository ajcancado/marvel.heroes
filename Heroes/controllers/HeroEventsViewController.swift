//
//  HeroEventsViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 02/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class HeroEventsViewController: GenericViewController, EmptyBackgroundProtocol {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var heroId : Int!
    
    var events: [CharacterEvent] = []
    
    var offset: Int = 0
    var limit: Int!
    var totalItems: Int!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        showHud()
        
        setupEmptyBackground()
        
        getHeroEvents()
    }
    
    func setupEmptyBackground(){
        
        let emptyBackgroundView = EmptyBackgroundView(image: UIImage(), top: Constants.Messages.emptyTopMessageEvents, bottom: Constants.Messages.emptyBottomMessageEvents)
        collectionView.backgroundView = emptyBackgroundView
        collectionView.backgroundView?.hidden = true
    }

    private func getHeroEvents(){
        
        Alamofire.request(HeroesRouter.getHeroEvents(id: heroId, offset: offset))
            .responseJSON { response in
                
                switch response.result {
                    
                case .Success(let json):
                    
//                    print(json)
                    
                    let eventsResponse = Mapper<EventsResponse>().map(json)
                    
                    self.offset = eventsResponse!.offset ?? 0
                    
                    self.limit = eventsResponse!.limit
                    
                    self.totalItems = eventsResponse!.total
                    
                    self.events.appendContentsOf(eventsResponse!.results)
                    
                    if self.events.count == 0 {
                        
                        self.collectionView.backgroundView?.hidden = false
                        
                    } else {
                        
                        self.collectionView.backgroundView?.hidden = true
                        
                        self.collectionView.dataSource = self
                        self.collectionView.delegate = self
                        
                        self.collectionView.reloadData()
                    }
                    
                    super.hideHud()
                    
                    
                case .Failure(let error):
                    
                    super.hideHud()
                    
                    
                    super.showHud(error.localizedDescription)
                    
                }
        }
    }

}

// MARK: - UICollectionViewDataSource

extension HeroEventsViewController : UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let cellSpacing = (collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing

        let cellWidth = (collectionViewLayout as! UICollectionViewFlowLayout).itemSize.width

        let cellCount = collectionView.numberOfItemsInSection(section)

        var inset = (collectionView.bounds.size.width - (CGFloat.init(integerLiteral: cellCount) * (cellWidth + cellSpacing))) * 0.5;

        inset = max(inset, 0.0)

        return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let row = indexPath.row
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("EventCellID", forIndexPath: indexPath) as UICollectionViewCell
        
        let image = cell.viewWithTag(1) as! UIImageView
        
        let event = events[row]
        
        image.kf_setImageWithURL(NSURL(string: event.getThumbailURL()),
                                 placeholderImage: nil,
                                 optionsInfo: nil,
                                 completionHandler: nil)
        
        
        return cell
        
    }
    
}

extension HeroEventsViewController: UICollectionViewDelegate{
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let row = indexPath.row
        
        let event = events[row]
        
        let optionMenu = UIAlertController(title: event.title, message: event.eventDescription, preferredStyle: .Alert)
        
        let actionOk = UIAlertAction(title: "OK", style: .Default, handler:nil)
    
        optionMenu.addAction(actionOk)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath){
        
        let row = indexPath.row
        
        if  row == self.events.count - 1 && self.totalItems != self.events.count {
            
            offset += limit
            
            getHeroEvents()
        }
        
        
    }
    
}
