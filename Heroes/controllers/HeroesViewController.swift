//
//  ViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import ObjectMapper


class HeroesViewController: GenericViewController, EmptyBackgroundProtocol, FavoriteHeroProtocol {
    
    struct Filters {
    
        static let nameAsc = "name"
        static let nameDesc = "-name"
        static let modified = "modified"
    
    }

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive = false
    
    var characters: [Character] = []
    
    var searchArray: [Character] = []
    
    let favoriteManager = FavoritesManager.get
    
    var indexOfFavorites: [Int] = []
    var indexPathToFavorite: NSIndexPath!
    
    var offset: Int = 0
    var limit: Int!
    var totalItems: Int!
    
    var orderBy = Filters.nameAsc
    var searchName = ""
    
    let filterButton = UIButton()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupRightButton()
        
        showHud()
        
        getHeroes()
        
        setupEmptyBackground()
        
        findTextField(searchBar).delegate = self

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = Constants.Titles.heroesViewTitle
        
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.title = ""
    }
    
    private func setupRightButton(){
        
        filterButton.setImage(UIImage(named: "ic_short_by"), forState: .Normal)
        filterButton.frame = CGRectMake(0, 0, 25, 25)
        filterButton.addTarget(self, action: #selector(showOrderOptions(_:)), forControlEvents: .TouchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: filterButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func setupEmptyBackground() {
        
        let emptyBackgroundView = EmptyBackgroundView(image: UIImage(), top: Constants.Messages.emptyTopMessageCharacter, bottom: Constants.Messages.emptyBottomMessageCharacter)
        tableView.backgroundView = emptyBackgroundView
        tableView.backgroundView?.hidden = true
    
        
        self.tableView.registerNib(UINib(nibName: "HeroCell", bundle: nil), forCellReuseIdentifier: "HeroCellID")
        
    }

    private func getHeroes(){
        
        Alamofire.request(HeroesRouter.getHeroes(offset: offset, name: searchName ,orderBy: orderBy))
            .responseJSON { response in
                
                switch response.result {
                    
                case .Success(let json):
                    
//                    print(json)
                
                    let charactersResponse = Mapper<CharactersResponse>().map(json)
                    
                    if self.searchActive{
                        
                        self.searchArray.removeAll()
                        
                        self.searchArray.appendContentsOf(self.characters)
                        
                        self.characters.removeAll()
                        
                        self.characters.appendContentsOf(charactersResponse!.results)
                        
                        self.tableView.reloadData()
                        
                    }
                    else{
                    
                        self.offset = charactersResponse!.offset ?? 0
                        
                        self.limit = charactersResponse!.limit
                        
                        self.totalItems = charactersResponse!.total
                        
                        self.characters.appendContentsOf(charactersResponse!.results)
                        
                        if self.characters.count == 0 {
                            
                            self.tableView.separatorStyle = .None
                            self.tableView.backgroundView?.hidden = false
                            
                        } else {
                            
                            self.calculateIndexOfFavorites()
                            
                            self.tableView.separatorStyle = .SingleLine
                            self.tableView.backgroundView?.hidden = true
                            
                            self.tableView.dataSource = self
                            self.tableView.delegate = self
                            
                            self.tableView.reloadData()
                        }

                        
                    }
                    
                    super.hideHud()
                    self.removedInfiniteScrollViewInFooter()
                    
                    
                case .Failure(let error):
                    
                    super.hideHud()
                    self.removedInfiniteScrollViewInFooter()
                    
                    super.showHud(error.localizedDescription)
    
                }
        }
    }
    
    func calculateIndexOfFavorites(){
        
        indexOfFavorites.removeAll()
        
        if self.favoriteManager.characters.count > 0 {
            
            
            for favoriteCharacter in self.favoriteManager.characters {
                
                for i in (0..<self.characters.count) {
                    
                    if self.characters[i].name == favoriteCharacter.name {
                        indexOfFavorites.append(i)
                    }
                }
            }
        }
        
    }
    
    func showOrderOptions(sender: UIButton) {
        
        let optionMenu = UIAlertController(title: Constants.Titles.actionSheetTitle, message: nil, preferredStyle: .ActionSheet)
        
        let orderByDateModified = UIAlertAction(title: Constants.Titles.actionSheetOptionDate, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.offset = 0
            
            self.characters.removeAll()
            
            self.orderBy = Filters.modified
            
            self.showHud()
            
            self.getHeroes()
            
        })
        
        let orderByNameAsc = UIAlertAction(title: Constants.Titles.actionSheetOptionNameAsc, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.offset = 0
            
            self.characters.removeAll()
            
            self.orderBy = Filters.nameAsc
            
            self.showHud()
            
            self.getHeroes()
        })
        
        let orderByNameDesc = UIAlertAction(title: Constants.Titles.actionSheetOptionNameDesc, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.offset = 0
            
            self.characters.removeAll()
            
            self.orderBy = Filters.nameDesc
            
            self.showHud()
            
            self.getHeroes()
            
        })
        
        let cancel = UIAlertAction(title: Constants.Titles.actionSheetOptionCancel, style: .Destructive, handler:nil)
        
        optionMenu.addAction(orderByDateModified)
        optionMenu.addAction(orderByNameAsc)
        optionMenu.addAction(orderByNameDesc)
        
        if let presenter = optionMenu.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        else{
            optionMenu.addAction(cancel)
        }
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    private func showInfiniteScrollViewInFooter() {
        
        let bounds = UIScreen.mainScreen().bounds
        let width = bounds.size.width
        
        let footerView = UIView(frame: CGRectMake(0, 0, width, 70))
        footerView.backgroundColor = UIColor.clearColor()
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.center = CGPointMake(width/2 , footerView.bounds.size.height/2)
        actInd.color = UIColor(red: 0.0/255.0, green: 162.0/255.0, blue: 194.0/255.0, alpha: 1.0)
        
        actInd.startAnimating()
        
        footerView.addSubview(actInd)
        
        self.tableView.tableFooterView = footerView;
    }
    
    func removedInfiniteScrollViewInFooter(){
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    func favoriteCharacter(favoriteButton: UIButton){
        
        let row = favoriteButton.tag
        
        let character = characters[row]
        
        if favoriteButton.selected {
        
            favoriteManager.removeFavoriteChracter(character)
            
            favoriteButton.selected = false
            favoriteButton.setImage(UIImage(named: "ic_favorite_black"), forState: .Normal)
        }
        else{
            
            favoriteManager.saveFavoriteCharacter(character)
            
            favoriteButton.selected = true
            favoriteButton.setImage(UIImage(named: "ic_favorite_red"), forState: .Normal)
        }
        
        calculateIndexOfFavorites()
        
        tableView.reloadData()
    }
    
    func isFavoriteCharacter(character: Character) -> Bool{
        
        var isFavorite = false
        
        for i in (0..<favoriteManager.characters.count) {
            if favoriteManager.characters[i].name == character.name {
                
                isFavorite = true
                
            }
        }
        
        return isFavorite
        
    }
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
        if segue.identifier == "segueToHeroDetail" {
            
            let svc = segue.destinationViewController as! PagerViewController
            
            let row = tableView.indexPathForSelectedRow?.row
            let section = tableView.indexPathForSelectedRow?.section
            
            let hero: Character!
            
            if section == 0 {
                
                hero = favoriteManager.characters[row!]
                
                svc.delegate = nil
            }
            else{
            
                svc.index = row
                svc.delegate = self
                hero = characters[row!]
            }
            
            svc.isFavorite = isFavoriteCharacter(hero)

            
            svc.heroId = hero.id
        }
    }

}


// MARK: - UITableViewDataSouce

extension HeroesViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            
            if favoriteManager.characters.count > 0 {
            
                return Constants.Titles.favoriteCharacters
            }
            else{
                return ""
            }
        }
        
        return Constants.Titles.allCharacters
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            if favoriteManager.characters.count > 0 {
                
                return 18.0
            }
            else{
                
                return 0.1
            }
            
        }
        
        return 18.0
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            if favoriteManager.characters.count > 0 {
                
                return 18.0
            }
            else{
                
                return 0.1
            }
            
        }
        
        return 18.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
        
            if favoriteManager.characters.count > 0 {
                
                return favoriteManager.characters.count
            }
            else{
             
                return 0
            }
        }
        
        return characters.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let row = indexPath.row
        let section = indexPath.section
        
        let cell = tableView.dequeueReusableCellWithIdentifier("HeroCellID", forIndexPath: indexPath) as! HeroCell
        
        let character: Character!
        
        if section == 0 {
            
            character = favoriteManager.characters[row]
            
            cell.favoriteButton.hidden = true
        }
        else{
            
            character = characters[row]
            
            cell.favoriteButton.hidden = false
            
            
        }
        
        cell.heroThumbnail!.kf_setImageWithURL(NSURL(string: character.getThumbailURL()),
                                              placeholderImage: nil,
                                              optionsInfo: nil,
                                              completionHandler: nil
        )

        cell.heroName.text = character.name
        
        cell.favoriteButton.tag = row
        cell.favoriteButton.addTarget(self, action: #selector(favoriteCharacter(_:)), forControlEvents: .TouchUpInside)
        
        if isFavoriteCharacter(character) {
            
            cell.favoriteButton.selected = true
            cell.favoriteButton.setImage(UIImage(named: "ic_favorite_red"), forState: .Normal)
        }
        else{
            
            cell.favoriteButton.selected = false
            cell.favoriteButton.setImage(UIImage(named: "ic_favorite_black"), forState: .Normal)
        }
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension HeroesViewController : UITableViewDelegate{
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        performSegueWithIdentifier("segueToHeroDetail", sender: self)
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let row = indexPath.row
        
        if  self.searchActive == false && row == self.characters.count - 1 && self.totalItems != self.characters.count {
            
            offset += limit
            
            showInfiniteScrollViewInFooter()
            
            getHeroes()
        }
    }
    
}

// MARK: UIScrollViewDelegate

extension HeroesViewController: UIScrollViewDelegate{
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
}

extension HeroesViewController: UITextFieldDelegate {
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        
        searchActive = false;
        
        searchName = ""
        
        searchBar.resignFirstResponder()
        
        characters.removeAll()
        
        characters.appendContentsOf(searchArray)
        
        tableView.reloadData()
        
        
        return false
    }
    
}

// MARK: UISearchBarDelegate

extension HeroesViewController: UISearchBarDelegate{

    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        setupNoSearchingView()
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        setupSearchingView()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            
            setupNoSearchingView()
            
            
        }
    }
    
    func setupSearchingView() {
        
        searchActive = true;
        
        filterButton.hidden = true
        
        searchName = searchBar.text!
        
        searchBar.resignFirstResponder()
        
        showHud()
        
        getHeroes()

        
    }
    
    func setupNoSearchingView() {
        
        searchActive = false;
        
        filterButton.hidden = false
        
        searchName = ""
        
        searchBar.resignFirstResponder()
        
        characters.removeAll()
        
        characters.appendContentsOf(searchArray)
        
        tableView.reloadData()
        
    }

}
