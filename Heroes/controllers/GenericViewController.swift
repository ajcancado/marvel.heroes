//
//  GenericViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import PKHUD
    
class GenericViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showHud(){
        
        HUD.show(.Progress)
    }
    
    func showHud(message : String){
        
        HUD.flash(.Label(message), delay: 2.0)
    }
    
    func showSucessHudWithMessage(message: String){
        
        HUD.flash(.Success, delay: 1.0) { finished in
            
            HUD.flash(.Label(message), delay: 1.0)
        }
    }
    
    func hideHud(){
        
        HUD.hide()
    }
    
    func findTextField(control: UIView) -> UITextField {
        
        for subview in control.subviews {
            
            if subview is UITextField {
                return subview as! UITextField
            }
            else if subview.subviews.count > 0 {
                
                let view = findTextField(subview)
                
                return view
                
            }
            
        }
        
        return UITextField()
    }
}
