//
//  PagerViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 02/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit

class PagerViewController: UIViewController {

    enum TabIndex : Int {
        case detailViewController = 0
        case eventsViewController = 1
    }
    
    var currentViewController: UIViewController?
    var detailViewController : UIViewController?
    var eventsViewController : UIViewController?
    
    @IBOutlet weak var contentView: UIView!
    var segment: UISegmentedControl!
    
    var heroId : Int!
    var isFavorite = false
    var index: Int!
    
    var delegate: FavoriteHeroProtocol!
    
    var tabInitialize: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createSegmentControl()
        
        segment.selectedSegmentIndex = tabInitialize
        displayCurrentTab(tabInitialize)
        displayCurrentNavigationBarButtonItem(tabInitialize)
    }
    
    func favoriteHero(button: UIButton){
        
        if delegate != nil{
            
            delegate.favoriteCharacter(button)
        }
        
    }
    
    func createSegmentControl(){
        
        segment = UISegmentedControl(items: [Constants.Titles.segmentDetailsTitle, Constants.Titles.segmentEventsTitle])
        segment.frame = CGRectMake(0, 0, 200, 29)
        segment.tintColor = Constants.Colors.segmentControlTintColor
        segment.setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Gotham-Light", size: 15)!],
                                       forState: UIControlState.Normal)
        
        segment.addTarget(self, action: #selector(self.switchTabs(_:)), forControlEvents: .ValueChanged)
        
        self.navigationItem.titleView = segment
        
    }
    
    
    func switchTabs(sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
        displayCurrentNavigationBarButtonItem(sender.selectedSegmentIndex)
    }
    
    func displayCurrentTab(tabIndex: Int){
        
        let vc = viewControllerForSelectedSegmentIndex(tabIndex)
        
        self.addChildViewController(vc!)
        vc!.didMoveToParentViewController(self)
        
        vc!.view.frame = self.contentView.bounds
        self.contentView.addSubview(vc!.view)
        self.currentViewController = vc
        vc!.viewWillAppear(true)
        
    }
    
    func displayCurrentNavigationBarButtonItem(tabIndex: Int){
        
        self.navigationItem.rightBarButtonItem = nil
        
        if tabIndex == 0 {
            
            if !isFavorite {
                self.addButtonFavorite()
            }
        }
    }
    
    func addButtonFavorite(){
        
        let favoriteButton = UIButton()
        
        favoriteButton.tag = index
        favoriteButton.setImage(UIImage(named: "ic_favorite_gray"), forState: .Normal)
        favoriteButton.frame = CGRectMake(0, 0, 25, 25)
        favoriteButton.addTarget(self, action: #selector(self.favoriteHero(_:)), forControlEvents: .TouchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: favoriteButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func viewControllerForSelectedSegmentIndex(index: Int) -> UIViewController? {
        var vc: UIViewController?
        
        switch index {
            
        case TabIndex.detailViewController.rawValue :
            
            if detailViewController == nil {
                let view: HeroDetailViewController = storyboard?.instantiateViewControllerWithIdentifier("ViewDetailID") as! HeroDetailViewController
                
                view.heroId = self.heroId
                
                view.title = Constants.Titles.heroDetailViewTitle
                
                detailViewController = view
            }
            
            vc = detailViewController
        
            
        case TabIndex.eventsViewController.rawValue :
            
            if eventsViewController == nil {
                let view = storyboard?.instantiateViewControllerWithIdentifier("ViewEventsID") as! HeroEventsViewController
                
                view.heroId = self.heroId
                
                view.title = Constants.Titles.heroEventsViewTitle
                
                eventsViewController = view
            }
            
            vc = eventsViewController
            
        default:
            return nil
        }
        return vc
    }

  

}
