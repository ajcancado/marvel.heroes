//
//  HeroDetailViewController.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class HeroDetailViewController: GenericViewController {

    var heroId: Int!
    
    @IBOutlet weak var heroThumbnail: UIImageView!
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showHud()
        
        getHero()
    }


    private func getHero(){
        
        Alamofire.request(HeroesRouter.getHero(heroId))
            .responseJSON { response in
                
                switch response.result {
                    
                case .Success(let json):
                    
//                    print(json)
                    
                    let charactersResponse = Mapper<CharactersResponse>().map(json)
                    
                    if charactersResponse!.results != nil && charactersResponse!.results.count == 1 {
                        
                        let character = charactersResponse?.results[0]
                        
                        let url = character!.thumbnailPath + "." + character!.thumbnailExtension
                        
                        
                        self.heroThumbnail!.kf_setImageWithURL(NSURL(string: url),
                            placeholderImage: nil,
                            optionsInfo: nil,
                            completionHandler: nil
                        )
                        
                        self.heroName.text = character!.name
                        
                        if character!.heroDescription != "" {
                        
                            self.heroDescription.text = character!.heroDescription
                        }
                        else{
                            self.heroDescription.text = Constants.Messages.noDataAvailable
                        }
                        
                    }
                    else {
                        
                        self.heroDescription.text = Constants.Messages.noDataAvailable
                    }
                    
                    super.hideHud()
                    
                    
                case .Failure(let error):
                    
                    super.hideHud()
                    
                    super.showHud(error.localizedDescription)
                    
                }
        }
    }
    
    

    
}
