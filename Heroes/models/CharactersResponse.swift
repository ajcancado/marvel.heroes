//
//  CharactersResponse.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import ObjectMapper

class CharactersResponse: Mappable {
    
    var offset: Int!
    var limit: Int!
    var total: Int!
    var count: Int!
    var results: [Character]!
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        offset              <- map[Constants.Fields.Response.offset]
        limit               <- map[Constants.Fields.Response.limit]
        total               <- map[Constants.Fields.Response.total]
        count               <- map[Constants.Fields.Response.count]
        results             <- map[Constants.Fields.Response.results]
        
    }

}
