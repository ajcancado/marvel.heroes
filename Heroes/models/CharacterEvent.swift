//
//  CharacterEvent.swift
//  Heroes
//
//  Created by Arthur Cançado on 01/10/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import ObjectMapper

class CharacterEvent : NSObject, Mappable {
    
    var id: Int!
    var title: String!
    var eventDescription: String!
    var thumbnailPath: String!
    var thumbnailExtension: String!
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                      <- map[Constants.Fields.Event.id]
        title                   <- map[Constants.Fields.Event.title]
        eventDescription        <- map[Constants.Fields.Event.description]
        thumbnailPath           <- map[Constants.Fields.Event.thumbnailPath]
        thumbnailExtension      <- map[Constants.Fields.Event.thumbnailExtension]

    }
    
    func getThumbailURL() -> String {
        return thumbnailPath + "." + thumbnailExtension
    }
}