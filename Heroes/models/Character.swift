//
//  Character.swift
//  Heroes
//
//  Created by Arthur Cançado on 30/09/16.
//  Copyright © 2016 Arthur Cançado. All rights reserved.
//

import UIKit
import ObjectMapper

class Character: NSObject, NSCoding, Mappable {

    
    var id: Int!
    var name: String!
    var heroDescription: String!
    var thumbnailPath: String!
    var thumbnailExtension: String!
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id                      <- map[Constants.Fields.Character.id]
        name                    <- map[Constants.Fields.Character.name]
        heroDescription         <- map[Constants.Fields.Character.description]
        thumbnailPath           <- map[Constants.Fields.Character.thumbnailPath]
        thumbnailExtension      <- map[Constants.Fields.Character.thumbnailExtension]
    }
    
    func getThumbailURL() -> String {
        return thumbnailPath + "." + thumbnailExtension
    }
    
    // MARK: NSCoding
    
    required init(coder unarchiver: NSCoder) {
        super.init()
        id = unarchiver.decodeObjectForKey(Constants.Fields.Character.id) as! Int
        name = unarchiver.decodeObjectForKey(Constants.Fields.Character.name) as! String
        heroDescription = unarchiver.decodeObjectForKey(Constants.Fields.Character.description) as! String
        thumbnailPath = unarchiver.decodeObjectForKey(Constants.Fields.Character.thumbnailPath) as! String
        thumbnailExtension = unarchiver.decodeObjectForKey(Constants.Fields.Character.thumbnailExtension) as! String
    }
    
    func encodeWithCoder(archiver: NSCoder) {
        archiver.encodeObject(id, forKey: Constants.Fields.Character.id)
        archiver.encodeObject(name, forKey: Constants.Fields.Character.name)
        archiver.encodeObject(heroDescription, forKey: Constants.Fields.Character.description)
        archiver.encodeObject(thumbnailPath, forKey: Constants.Fields.Character.thumbnailPath)
        archiver.encodeObject(thumbnailExtension, forKey: Constants.Fields.Character.thumbnailExtension)
    }
    
}
